# Changelog

Changelog has been moved to https://doc.ozforensics.com/oz-knowledge/guides/developer-guide/sdk/oz-mobile-sdk/ios/changelog

#### 6.x
- `6.4.x`  - [6.4.0](#640)

## [6.4.1]
Release 15.06.2022

#### Corrected
- Improved stability

## [6.4.0]
Release 21.05.2022

#### Updated
- updated versioning to synchronize with Android

#### 3.x
- `3.0.x`  - [3.0.0](#300)

## [3.0.0]
Release 21.04.2022

#### Corrected
- Improved stability

## [3.0.0]
Release 22.02.2022

#### Added
- Added a new simplified analysis structure – AnalysisRequest.
* Creates the AnalysisRequest instance: AnalysisRequestBuilder(_ folderId: String?)
* Adds analyzes: AnalysisRequest.addAnalysis(_ analysis: Analysis)
* Uploads media to a folder: AnalysisRequest.addMedia(_ media: OZMedia)
* Adds metadata to a folder: AnalysisRequest.addFolderMeta(_ meta: [String: Any])
* Applies the requested analyzes: AnalysisRequest.run(scenarioProgressHandler: @escaping ((_ state: ScenarioState) -> Void),
             uploadProgressHandler: @escaping ((AnalysisStatus) -> Void),
             completionHandler: @escaping (_ resolutions : AnalysisResult?, _ error: Error?) -> Void)

#### 2.x
- `2.3.x`  - [2.3.0](#230) 
- `2.2.x`  - [2.2.0](#220)
- `2.1.x`  - [2.1.0](#210)
- `2.0.x`  - [2.0.0](#200)

## [2.3.0]
Release 17.01.2022

#### Added
- Added methods of on-device analysis: runOnDeviceLivenessAnalysis and runOnDeviceBiometryAnalysis.
- You can choose the installation version. Standard installation gives access to full functionality. The core version (OzLivenessSDK/Core) installs SDK without the on-device functionality.
- Added a method to upload data to server and start analyzing it immediately: uploadAndAnalyse.
- Improved the licensing process, now you can add a license when initializing SDK: OZSDK(licenseSources: [LicenseSource], completion: @escaping ((LicenseData?, LicenseError?) -> Void)), where LicenseSource is a path to physical location of your license, LicenseData contains the license information.
- Added the setLicense method to force license adding.

#### Corrected
- Improved stability

## [2.2.3]
Release 29.10.2021

#### Added
- Added Turkish locale

#### Corrected
- Improved stability

## [2.2.1]
Release 22.10.2021. 

#### Added
- Added Kyrgyz locale
- Added Completion Handler for analysis results.
- Added Error User Info to telemetry to show detailed info in case of an analysis error.

#### Corrected
- Improved stability

## [2.2.0]
Release 03.09.2021. 

#### Added
- Added local on-device analysis.
- Added oval and rectangular frames.

#### Updated
- Added Xcode 12.5.1+ support.

#### Corrected
- Improved stability

## [2.1.4]
Release 02.08.2021. 

#### Added
- Added SDK configuration with licences.

## [2.1.3]
Release 06.07.2021. 

#### Added
- Added the One Shot gesture.
- Improved OZVerificationResult: added bestShotURL which contains the best shot image and preferredMediaURL which contains an URL to the best quality video.

#### Updated
- When performing a local check, you can now choose a front or back camera.

## [2.1.2]
Release 25.05.2021. 

#### Added
- Authorization sessions extend automatically

#### Updated
-  Updated authorization interfaces

## [2.1.1]
Release 18.05.2021. 

#### Added
- Added Kazakh locale
- Added license error texts

#### Added
- You can cancel network requests
- You can specify Bundle for license
- Added analysis parameterization documentAnalyse.

## [2.1.0]
Release 15.04.2021. 

#### Added
- You can cancel network requests
- You can specify Bundle for license
- Added analysis parameterization documentAnalyse.

#### Corrected
- Fixed building errors (Xcode 12.4 / Cocoapods 1.10.1)

## [2.0.0]
Release 05.03.2021. 

#### Added
- Added license support

#### Updated
- Added Xcode 12 support instead of 11.

#### Corrected
- Fixed the documentAnalyse error where you had to fill analyseStates to launch the analysis
- Fixed the logs collection process errors
- Improved stability
